import AppRouter from "./components/AppRouter";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import React from "react";

function App() {
  return (
    <div>
      <ToastContainer position="top-center" autoClose={1000} />
      <AppRouter />
    </div>
  );
}

export default App;
