import React from "react";
import {Route, Switch} from "react-router-dom";
import {routes} from "../router";

const AppRouter = () => {

  return (
    <Switch>
      {routes.map(route =>
        <Route key={route.path}
               exact={route.exact}
               path={route.path}
               component={route.component}
        />
      )}
      {/*<Redirect to={CHAT_PAGE}/>*/}
    </Switch>
)
  ;
};

export default AppRouter;
