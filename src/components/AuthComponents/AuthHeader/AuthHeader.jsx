import React from "react";

import s from './AuthHeader.module.scss';

const AuthHeader = ({desc}) => {
  return (
    <div className={s.authHeader}>
      <span className={s.authHeader__title}>WEHELP</span>
      <p className={s.authHeader__desc}>{desc}</p>
    </div>
  );
};

export default AuthHeader;
