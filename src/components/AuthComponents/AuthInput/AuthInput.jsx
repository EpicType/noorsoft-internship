import React from "react";

import s from "./AuthInput.module.scss";

const AuthInput = ({type, placeholder, register, options}) => {
  return (
    <div className={s.authInput}>
      <input
        className={s.authInput__input}
        type={type}
        placeholder={placeholder}
        {...options}
      />
    </div>
  );
};

export default AuthInput;
