import React from "react";

import s from "./AuthWrapper.module.scss";

const AuthWrapper = ({children}) => {
  return (
    <div className={s.authWrapper}>
      <div className={s.authContainer}>
        {children}
      </div>
    </div>
  );
};

export default AuthWrapper;
