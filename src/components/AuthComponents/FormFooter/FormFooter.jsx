import React from "react";

import s from "./FormFooter.module.scss";
import {NavLink} from "react-router-dom";

const FormFooter = ({text, textLink, path}) => {
  return (
    <div className={s.formFooter}>
      <span>{text}</span>
      <NavLink className={s.formFooter__link} to={path}>
        {textLink}
      </NavLink>
    </div>
  );
};

export default FormFooter;
