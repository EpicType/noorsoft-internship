import React from "react";
import cn from "classnames";
import s from "./SubmitButton.module.scss";

const SubmitButton = ({isLoading, text}) => {
  return (
    <button className={cn(s.submitButton, {[s.submitButton__loading]: isLoading})} type="submit">
      {text}
    </button>
  );
};

export default SubmitButton;
