import React from "react";
import {NavLink, useHistory} from "react-router-dom";

import s from "./DialogHeader.module.scss";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEllipsisV} from "@fortawesome/free-solid-svg-icons";
import {UncontrolledTooltip} from "reactstrap";


const DialogHeader = () => {
  const history = useHistory();

  return (
    <div className={s.dialogHeader}>
      <NavLink to="/" className={s.dialogHeader__title}>WEHELP</NavLink>
      <div onClick={() => history.push("/settings")} id={"setting"} className={s.dialogHeader__menu}>
        <FontAwesomeIcon icon={faEllipsisV} className={s.dialogHeader__item} size="1x"/>
      </div>
      <UncontrolledTooltip placement={"right"} target={"setting"}>
        <p className={s.dialogHeader__tooltip}>Settings</p>
      </UncontrolledTooltip>
    </div>
  );
};

export default DialogHeader;
