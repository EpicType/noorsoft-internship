import React, {useState} from "react";

import s from "./DialogMenu.module.scss";
import DialogHeader from "./DialogHeader/DialogHeader";
import NavItem from "./NavItem/NavItem";
import cn from "classnames";

const DialogMenu = ({isOpen}) => {


  return (
    <div className={cn(s.dialogMenu, {[s.active]: isOpen})}>

      <DialogHeader />
      <div className={s.dialogs}>
        <ul className={s.dialogs__list}>
          <NavItem status="all" dialogName="All Dialogs" />
          <NavItem status="active" dialogName="Active Dialogs" />
          <NavItem status="completed" dialogName="Completed Dialogs" />
          <NavItem status="saved" dialogName="Saved Dialogs" />
        </ul>
      </div>
    </div>
  );
};

export default DialogMenu;
