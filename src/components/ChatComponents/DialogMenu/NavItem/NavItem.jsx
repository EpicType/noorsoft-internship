import React from "react";

import s from "./NavItem.module.scss";

import {NavLink} from "react-router-dom";

const NavItem = ({status, dialogName, count}) => {
  return (
    <li className={s.dialogItem}>
      <NavLink activeClassName={s.dialogItem__link__active} className={s.dialogItem__link} to={"/dialogs/" + status}>
        <div className={s.dialogInfo}>
          <h3 className={s.dialogInfo__name}>{dialogName}</h3>
        </div>
        <div className={s.dialogInfo__indicator}>
          <span className={s.indicator__text}>{count}</span>
        </div>
      </NavLink>
    </li>
  );
};

export default NavItem;
