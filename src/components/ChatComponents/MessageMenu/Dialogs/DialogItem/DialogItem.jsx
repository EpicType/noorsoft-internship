import React from "react";
import {NavLink} from "react-router-dom";
import cn from "classnames";

import s from "./DialogItem.module.scss";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBookmark as solidBookmark} from "@fortawesome/free-solid-svg-icons";
import {faBookmark as regularBookmark} from "@fortawesome/free-regular-svg-icons";
import {faStar as regStar} from "@fortawesome/free-regular-svg-icons";
import {faStar as solStar} from "@fortawesome/free-solid-svg-icons";


const DialogItem = ({dialog, handleSaveStatusChange}) => {

  let fillStars = [];
  let regularStars = [];


  for (let i = 0; i < dialog?.star; i++) {
    fillStars.push(i);
  }

  for (let i = 0; i < (5 - dialog?.star); i++) {
    regularStars.push(i);
  }


  return (
    <li className={s.dialogsItem}>
      <NavLink className={s.dialogInfo__link} to={"/dialog/" + dialog?._id}>
        <div className={s.dialogInfo}>
          <img className={s.dialogInfo__userAvatar} src={dialog?.userAvatar}
               alt="#"/>
          <div className={s.dialogInfo__userName}>
            {dialog?.userName}
          </div>
        </div>
        <div className={s.dialogInfo__textContainer}>
          <div className={s.dialogInfo__text}>
            {dialog?.recentMessage?.message}
          </div>
        </div>
        <div className={s.dialogInfo__nav}>
          <button
            className={s.dialogInfo__nav__button}
            onClick={(e) => handleSaveStatusChange(e, dialog?._id, dialog?.saved)}
          >
            <FontAwesomeIcon
              icon={dialog?.saved ? solidBookmark : regularBookmark}
              size="2x"
              className={cn({[s.dialogInfo__nav__delete]: dialog?.saved}, {[s.dialogInfo__nav__save]: !dialog?.saved})}
            />
          </button>
          <div className={s.dialogInfo__rating}>
            {fillStars && fillStars.map((el, index) =>
              <FontAwesomeIcon key={index} icon={solStar} className={s.dialogInfo__rating__star}/>
            )}
            {regularStars && regularStars.map((el, index) =>
              <FontAwesomeIcon key={index} icon={regStar} className={s.dialogInfo__rating__star}/>
            )}
          </div>
          <div className={s.dialogInfo__nav__date}>
            {dialog?.createdAt}
          </div>
        </div>
      </NavLink>
    </li>
  );
};


export default DialogItem;
