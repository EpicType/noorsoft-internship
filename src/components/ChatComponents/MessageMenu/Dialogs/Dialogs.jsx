import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";

import s from "./Dialogs.module.scss";

import {db} from "../../../../firebase/firebase";
import {GET_DIALOGS_REQUEST} from "../../../../store/reducers/chat/consts";
import {
  editDialogStatus,
  editIsOpen,
  setFoundedDialogsList,
  setFoundedMessagesList
} from "../../../../store/reducers/chat/action-creators";
import {faSearch} from "@fortawesome/free-solid-svg-icons/faSearch";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import DialogItem from "./DialogItem/DialogItem";
import MessageItem from "./MessageItem/MessageItem";

const Dialogs = () => {
  const dispatch = useDispatch();

  const {status} = useParams();

  const dialogsList = useSelector(state => state.chat.dialogsList);
  const foundedDialogsList = useSelector(state => state.chat.foundedDialogsList);
  const foundedMessagesList = useSelector(state => state.chat.foundedMessagesList);
  const isLoading = useSelector(state => state.chat.isLoading);
  const userID = useSelector(state => state.auth.userID);

  const [searchValue, setSearchValue] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const [alMessage, setAlMessage] = useState("");

  useEffect(() => {
    dispatch(editIsOpen(false));
    dispatch({type: GET_DIALOGS_REQUEST, payload: {status, userID}});
    dispatch(setFoundedDialogsList([]));
    setIsOpen(prevState => false);
    setSearchValue("");


    // debugger;
    // let dialogsID;
    // let dialogsList = []
    //
    // db.ref("/users/" + userID + "/Dialogs/").once("value", snapshot => {
    //   console.log("АЙДИ ДИАЛОГОВ",snapshot.val());
    //   dialogsID = snapshot.val();
    // }).then(() => {
    //   const promise = dialogsID.map(key => {
    //     return db.ref("dialogs").child(key).once("value");
    //   });
    //   Promise.all(promise).then((snapshot) => {
    //     snapshot.forEach((snap) => {
    //       dialogsList.push(snap.val())
    //     })
    //     console.log("Ну вот тебе и диалоги", dialogsList)
    //   })
    // })


  }, [status]);

  useEffect(() => {
    debugger;
    if (searchValue) {
      // const searchInputToLower = searchValue.toString().toLowerCase();
      // const searchInputToUpper = searchValue.toString().toUpperCase();

      const filteredDialog = dialogsList.filter(item => item.userName.includes(searchValue));
      dispatch(setFoundedDialogsList(filteredDialog));
      // setFilteredDialog(filteredDialog)

      const filteredMessage = alMessage.filter(item => item.message.includes(searchValue));
      dispatch(setFoundedMessagesList(filteredMessage))

      // db.ref("/dialogs/" + dialogID).orderByChild("message")
      //   .startAt(searchInputToUpper).endAt(searchInputToLower + "\uf88ff")
      //   .once("value", snapshot => {
      //   let searchMessages = [];
      //   snapshot.forEach((snap) => {
      //     searchMessages.push(snap.val());
      //   });
      //   console.log("НАЙДЁННОЕ СООБЩЕНИЕ", searchMessages);
      // });
    }
  }, [searchValue]);


  const handleInputFocus = () => {
    setIsOpen(prevState => true);

    let dialogsID = [];
    dialogsList.map(el => (
      dialogsID.push(el._id)
    ));

    const allMessage = [];
    const promise = dialogsID.map(id => {
      return db.ref("/dialogsMessage/" + id + "/messages/").once("value");
    });
    Promise.all(promise)
      .then((snapshot) => {
        snapshot.forEach((snap) => {
          // allMessage.push(snap.val());
          snap.forEach((el) => {
            allMessage.push(el.val())
          })
        });
        console.log("ВСЕ СООБЩЕНИЯ", allMessage);
        // dispatch(setFoundedMessagesList(allMessage))
        setAlMessage(allMessage)
      });
  };

  const handleInputBlur = () => {
    if (!searchValue) {
      setIsOpen(prevState => false);
    }
  };

  const handleSaveStatusChange = (e, dialogID, saved) => {
    e.preventDefault();
    const newSavedStatus = !saved;
    db.ref("/dialogs/" + dialogID).update({
      saved: newSavedStatus
    })
      .then(() => {
        console.log("successful");
        dispatch(editDialogStatus(dialogID, newSavedStatus));
      });
  };


  if (isLoading) {
    return (
      <h2>LOADING DIALOGS...</h2>
    );
  }

  return (
    <div className={s.dialogs}>
      <div className={s.search}>
        <div className={s.search__wrapper}>
          <input
            className={s.search__input}
            type="text"
            value={searchValue}
            onFocus={handleInputFocus}
            onBlur={handleInputBlur}
            onChange={(e) => setSearchValue(e.target.value)}
          />
          <FontAwesomeIcon icon={faSearch} className={s.search__icon}/>
        </div>
        {status === "all" && <h2>Человек в очереди: {dialogsList.length}</h2> }
      </div>
      {isOpen
        ?
          <ul className={s.dialogsList}>
            <li>
              Диалоги
            </li>
            {foundedDialogsList.length > 0 && foundedDialogsList.map((item) => (
              <DialogItem handleSaveStatusChange={handleSaveStatusChange} key={item._id} dialog={item}/>
            ))}
            <li>
              Сообщения
            </li>
            {foundedMessagesList.length > 0 && foundedMessagesList.map((item) => (
              // <li key={item._id}>{item.message}</li>
              <MessageItem key={item._id} messageInfo={item} />
            ))}
          </ul>
        :
        <ul className={s.dialogsList}>
          {dialogsList.length > 0 ? dialogsList.map((item) => (
            <DialogItem handleSaveStatusChange={handleSaveStatusChange} key={item._id} dialog={item}/>
          )) : <h3>You have not any dialog</h3>}
        </ul>}
    </div>
  );
};

export default Dialogs;
