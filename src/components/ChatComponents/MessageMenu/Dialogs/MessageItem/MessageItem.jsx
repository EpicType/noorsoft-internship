import React, {useEffect, useState} from "react";

import s from "./MessageItem.module.scss";
import {db} from "../../../../../firebase/firebase";
import {useHistory} from "react-router-dom";

const MessageItem = ({messageInfo}) => {
  const history = useHistory();

  const [userInfo, setUserInfo] = useState([]);


  useEffect(() => {
    debugger;
    db.ref("/users/" + messageInfo.from_id).once("value", snapshot => {
      setUserInfo(snapshot.val())
    })
  }, [])

  return (
    <li className={s.messageItem} onClick={() => history.push("/dialog/" + messageInfo.dialogID)}>
      <div className={s.messageItem__info}>
        <img className={s.messageItem__img} src={userInfo?.photoURL} alt="userAvatar"/>
        <div className={s.messageItem__userName}>
          {userInfo?.displayName}
        </div>
      </div>
      <div className={s.messageItem__message}>
        {messageInfo.message}
      </div>
    </li>
  );
};

export default MessageItem;
