import React from "react";
import {useDispatch} from "react-redux";

import s from "./MessageHeader.module.scss";

import {SET_EXIT_REQUEST} from "../../../../store/reducers/auth/consts";

const MessageHeader = ({userEmail}) => {
  const dispatch = useDispatch();

  const handleExit = () => {
    dispatch({type: SET_EXIT_REQUEST});
  }

  return (
    <div className={s.messageHeader}>
      <div className={s.userInfo}>
        <div className={s.userInfo__status}/>
        <div className={s.userInfo__name}>
          <span>{userEmail}</span>
        </div>
      </div>
      <div className={s.userActions}>
        {/*<div className={s.userActions__call}>*/}
        {/*  <div className={s.userActions__call__img}/>*/}
        {/*</div>*/}
        {/*<div className={s.userActions__videoCall}>*/}
        {/*  <div className={s.userActions__videoCall__img}/>*/}
        {/*</div>*/}
        <button onClick={() => handleExit()} className={s.userActions__exit}>
          Exit
        </button>
      </div>
    </div>
  );
};

export default MessageHeader;
