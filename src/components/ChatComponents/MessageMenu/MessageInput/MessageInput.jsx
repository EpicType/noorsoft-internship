import React from "react";

import s from "./MessageInput.module.scss";

import Picker from "emoji-picker-react";

const MessageInput = ({handleSendMessage, userID, text, setText, dialogID, setShowPicker, showPicker}) => {

  const onEmojiClick = (event, emojiObject) => {
    setText(prevInput => prevInput + emojiObject.emoji);
  };

  return (
    <div className={s.messageInput}>
      <textarea
        onChange={(e) => setText(e.target.value)}
        value={text}
        placeholder="Type your message here..."
        className={s.messageInput__input}
      />
      <div className={s.messageNav}>
        <div className={s.messageNav__smiles}>
          <div className={s.messageNav__smiles__img} onClick={() => setShowPicker(val => !val)}/>
        </div>
        {showPicker && <Picker
          pickerStyle={{ position: "absolute", bottom: "66px", right: "237px" }}
          onEmojiClick={onEmojiClick} />}
        <div className={s.messageNav__attach}>
          <div className={s.messageNav__attach__img} />
        </div>
        <button disabled={!text} onClick={() => handleSendMessage(userID, dialogID)} className={s.messageNav__send}>
          <div className={s.messageNav__send__img}/>
        </button>
      </div>
    </div>
  );
};

export default MessageInput;
