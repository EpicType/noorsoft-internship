import React from "react";

import s from "./MessageCompanionItem.module.scss";

const MessageInterlocutorItem = ({message}) => {
  return (
    <li className={s.messageItem}>
      <div className={s.messageInfo}>
        <img className={s.messageInfo__avatar} src={message.userAvatar}
             alt="#"/>
        <div className={s.messageInfo__date}>
          10:25 Am
        </div>
      </div>
      <div className={s.messageContainer}>
        <p className={s.message}>
          {message.message}
        </p>
      </div>
    </li>
  );
};

export default MessageInterlocutorItem;
