import React, {useEffect, useRef, useState} from "react";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import s from "./MessageList.module.scss";

import {db} from "../../../../firebase/firebase";
import {SEND_MESSAGE_REQUEST} from "../../../../store/reducers/chat/consts";

import {editIsOpen, setMessagesList} from "../../../../store/reducers/chat/action-creators";
import MessageInput from "../MessageInput/MessageInput";
import MessageInterlocutorItem from "../MessageInterlocutorItem/MessageCompaionItem";
import useInterval from "../../../../hooks/useInterval";
import MessageOwnerItem from "../MessageOwnerItem/MessageOwnerItem";
import {useTypedSelector} from "../../../../hooks/useTypedSelector";
import {IDialog, IMessage} from "../../../../types/types";

interface ParamTypes {
  dialogID: string,
}

const MessageList: React.FC = () => {
  const dispatch = useDispatch();

  const userID = useTypedSelector(state => state.auth.userID);
  const photoURL = useTypedSelector(state => state.auth.photoURL);
  const isOpen = useTypedSelector(state => state.chat.isOpen);
  const messagesList = useTypedSelector(state => state.chat.messagesList);
  const userName = useTypedSelector(state => state.auth.userName);

  const { dialogID } = useParams<ParamTypes>()

  const messagesEndRef = useRef<HTMLDivElement>(null);

  const [inputValue, setInputValue] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [dialogStatus, setDialogStatus] = useState<string | undefined>("");
  const [isLoading, setIsLoading] = useState(false);
  const [showPicker, setShowPicker] = useState(false);

  // const [messages, setMessages] = useState("");
  const scrollToBottom = () => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
    }
  };

  useEffect(() => {
    debugger;
    setIsLoading(prevState => true);
    db.ref("/dialogsMessage/" + dialogID + "/messages/").once("value", snapshot => {
      let messages: IMessage[] | null = [];
      console.log(snapshot);
      snapshot.forEach((snap) => {
        messages?.push(snap.val());
      });
      console.log(messages);
      dispatch(setMessagesList(messages));
      // setMessages(messages);
    }).then(() => {
      let dialogInfo: IDialog | null = null;
      db.ref("/dialogs/" + dialogID).once("value", snapshot => {
        dialogInfo = snapshot.val();
        // setDialogStatus(dialogInfo.status)
      }).then(() => {
        setDialogStatus(dialogInfo?.status);
        setIsLoading(prevState => false);
        scrollToBottom();
      });
    });

    //TODO: Присвоение диалога пользователем.

    db.ref("/dialogs/" + dialogID).update({
      operatorId: userID,
    });
    dispatch(editIsOpen(false));

  }, [dialogID]);

  useInterval(() => {
    db.ref("/dialogsMessage/" + dialogID + "/messages/").once("value", snapshot => {
      let messages: IMessage[] | null = [];
      console.log(snapshot);
      snapshot.forEach((snap) => {
        messages?.push(snap.val());
      });
      console.log("Периодический запрос на сообщения", messages);
      dispatch(setMessagesList(messages));
      // setMessages(messages);
    });

  }, 30000);




  const handleSendMessage = (userID: string, dialogID: string) => {
    setShowPicker(false);
    const data = {
      dialogID: dialogID,
      userID: userID,
      inputValue: inputValue,
      userAvatar: photoURL,
      userName: userName,

    };
    dispatch({type: SEND_MESSAGE_REQUEST, payload: data});
    setInputValue("");
  };

  if (isLoading) {
    return (
      <h3>LOADING MESSAGES</h3>
    );
  }

  return (
    <>
      <ul className={s.messageList}>
        {messagesList.length > 0 ? messagesList.map((message: IMessage) => {
            return message.from_id === userID ? <MessageOwnerItem key={message._id} photoURL={photoURL} message={message}/> :
              <MessageInterlocutorItem key={message._id} message={message}/>;
          }
        ) : <h2>You haven't eny messages</h2>}
        <div ref={messagesEndRef}  />
      </ul>
      {dialogStatus === "completed" ? <h2>completed</h2> : <MessageInput
        setText={setInputValue}
        text={inputValue}
        handleSendMessage={handleSendMessage}
        userID={userID}
        dialogID={dialogID}
        setShowPicker={setShowPicker}
        showPicker={showPicker}
      />}
    </>
  );
};

export default MessageList;
