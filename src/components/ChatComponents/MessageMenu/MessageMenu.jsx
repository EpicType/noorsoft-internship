import React from "react";
import {Route} from "react-router-dom";

import s from "./MessageMenu.module.scss";

import MessageHeader from "./MessageHeader/MessageHeader";
import Dialog from "./Dialogs/Dialogs";
import MessageList from "./MessageList/MessageList";
import Settings from "../Settings/Settings";
import cn from "classnames";

const MessageMenu = ({userEmail, userID, isOpen}) => {


  // const handleSendMessage = (userID, text) => {
  //
  //   const newMessageKey = db.ref().child("dialogs").push().key;
  //
  //   console.log(newMessageKey);
  //   const message = {
  //     _id:newMessageKey,
  //     username:"EpicType",
  //     email:"test@mail.ru",
  //     message:text,
  //   };
  //   const updates = {};
  //   updates["/dialogs/" + newMessageKey] = message;
  //   updates["/users/" + userID + "/dialogs/" + newMessageKey] = newMessageKey;
  //   console.log(userID);
  //   return db.ref().update(updates);
  // };

  return (
    <div className={cn(s.messageMenu, {[s.active]: isOpen})}>
      <MessageHeader userEmail={userEmail}/>
      <Route exact path="/" component={Plug} />
      <Route exact path="/dialog/:dialogID?" component={MessageList}/>
      <Route exact path="/dialogs/:status?" component={Dialog}/>
      <Route exact path="/settings" component={Settings}/>
    </div>
  );
};

export const Plug = () => {
  return (
    <div className={s.messagePlug}>
      <p className={s.messagePlug__text}>Welcome to the WECHAT app. Select a dialog to get started.</p>
    </div>
  );
};


export default MessageMenu;
