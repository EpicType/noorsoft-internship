import React from "react";
import s from "./MessageOwnerItem.module.scss";
import moment from "moment";
import {IMessage} from "../../../../types/types";

interface MessageOwnerItemProps {
  message: IMessage,
  photoURL: string | undefined
}

const MessageOwnerItem: React.FC<MessageOwnerItemProps> = ({message, photoURL}) => {
  return (
    <li className={s.messageItem}>
      <div className={s.messageContainer}>
        <p className={s.message}>
          {message.message}
        </p>
      </div>
      <div className={s.messageInfo}>
        <img className={s.messageInfo__avatar}
             src={photoURL}
             alt="#"/>
        <div className={s.messageInfo__date}>
          {moment.utc(message.startedAt).local().format("ddd, hA")}
        </div>
      </div>
    </li>
  );
};

export default MessageOwnerItem;
