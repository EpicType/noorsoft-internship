import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import cn from "classnames";

import s from "./Settings.module.scss";

import {auth, db, storage} from "../../../firebase/firebase";
import {setUserName} from "../../../store/reducers/auth/action-creators";

const Settings = () => {
  const dispatch = useDispatch();
  const userID = useSelector(state => state.auth.userID);

  const [image, setImage] = useState("");
  const [name, setName] = useState("");

  const handleImageUpload = () => {
    if (name) {
      auth.currentUser.updateProfile({
        displayName: name
      })
        .then(() => {
          db.ref("/users/" + userID).update({
            displayName: name
          }).then(() => {
            dispatch(setUserName(name))
            alert("Successful");
          }).catch(() => {
            alert("wrong");
          });
        })
      setName("");
    }
    if (image) {
      const storageRef = storage.ref("/users/" + userID + "/userAvatar/");
      storageRef.put(image).then((snapshot) => {
        storageRef.getDownloadURL()
          .then((url) => {
            db.ref("/users/" + userID).update({
              photoURL: url
            }).then(() => {
              alert("Successful");
              auth.currentUser.updateProfile({
                photoURL: url
              })
                .then(() => {
                  alert("success");
                });
            }).catch(() => {
              alert("wrong");
            });
          });
      });
    }
  };

  return (
    <div className={s.settings}>
      <div className={s.settings__wrapper}>
        <div className={s.settings__header}>
          Обновить профиль
        </div>
        <div className={s.settingsName}>
          <label className={s.settingsName__label}>Name:</label>
          <input
            className={cn([s.settingsName__input], [s.input])}
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className={s.settingsAvatar}>
          <label className={s.settingsAvatar__label}>Avatar:</label>
          <div className={s.settingsAvatar__imgContainer}>
            <img className={s.settingsAvatar__img} alt="#"
                 src={image ? URL.createObjectURL(image) : "http://via.placeholder.com/300x150"}/>
          </div>
          <input
            className={s.settingsAvatar__input}
            type="file"
            onChange={(e) => setImage(e.target.files[0])}
          />
        </div>
        <div className={s.submitContainer}>
          <button onClick={handleImageUpload} className={s.submitContainer__button}>Refresh Profile</button>
        </div>
      </div>
    </div>
  );
};

export default Settings;
