import firebase from "firebase/compat";

export const firebaseConfig = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: process.env.REACT_APP_AUTHDOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PID,
  storageBucket: process.env.REACT_APP_SB,
  messagingSenderId: process.env.REACT_APP_SID,
  appId: process.env.REACT_APP_APPID,
  measurementId: process.env.REACT_APP_MID
}

export const firebase_app = firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
// export const auth = getAuth(firebase_app);
export const firestore = firebase.firestore();
export const db = firebase.database();
export const storage = firebase.storage();
// export const firestore = getFirestore(firebase_app);
