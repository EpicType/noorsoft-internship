import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";

import s from "./ChatPage.module.scss";

import {
  setAuth,
  setFetching,
  setUserID,
  setUserEmail,
  setUserPhoto,
  setUserName
} from "../../store/reducers/auth/action-creators";
import {auth} from "../../firebase/firebase";

import DialogMenu from "../../components/ChatComponents/DialogMenu/DialogMenu";
import MessageMenu from "../../components/ChatComponents/MessageMenu/MessageMenu";
import AuthService from "../../services/AuthService";
import cn from "classnames";
import {editIsOpen} from "../../store/reducers/chat/action-creators";

const ChatPage = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const userEmail = useSelector(state => state.auth.userEmail);
  const isAuth = useSelector(state => state.auth.isAuth);
  const isFetching = useSelector(state => state.auth.isFetching);
  const userID = useSelector(state => state.auth.userID);
  const isOpen = useSelector(state => state.chat.isOpen)

  const [isOut, setIsOut] = useState(false);
  // const [isOpen, setIsOpen] = useState('');

  useEffect(() => {
    dispatch(setFetching(true));
    auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch(setUserEmail(user.email));
        dispatch(setUserID(user.uid));
        dispatch(setUserPhoto(user.photoURL));
        dispatch(setAuth(true));
        dispatch(setUserName(user.displayName));
        console.log(user);
        // db.ref("dialogs").orderByChild("operatorId").equalTo(user.uid).on("value", snapshot => {
        //   let dialogs = [];
        //   snapshot.forEach((snap) => {
        //     dialogs.push(snap.val());
        //   });
        //   dispatch(setDialogsList(dialogs));
        //   console.log("СПИСОК ДИАЛОГОВ", dialogs);
        // });



        // db.ref("dialogs").orderByChild("operatorId").equalTo(null).on("value", snapshot => {
        //   let dialogs = [];
        //   snapshot.forEach((snap) => {
        //     dialogs.push(snap.val());
        //   });
        //   dispatch(setDialogsList(dialogs));
        //   console.log("DIALOGS LIST", dialogs);
        // })
        dispatch(setFetching(false));
      } else {
        history.push("/login");
        dispatch(setFetching(false));
      }
    });
  }, []);


  useEffect(() => {
    if (isOut) {
      dispatch(setFetching(true));
      AuthService.logout()
        .then(() => {
          setIsOut(false);
          history.push("/login");
        }).catch((err) => {
        setIsOut(false);
      }).finally(() => {
        dispatch(setFetching(false));
      });
    }
  }, [isOut]);


  if (isFetching) {
    return (
      <h1>LOADING APPLICATION...</h1>
    );
  }

  return (
    <>
      {isAuth && <div className={s.chatPage}>
        <DialogMenu isOpen={isOpen}/>
        <div onClick={() => dispatch(editIsOpen(!isOpen))} className={cn(s.dialogMenu__burger, {[s.active]: isOpen})}>
          <span className={cn(s.dialogMenu__burger__span, {[s.active]: isOpen})}/>
        </div>
        <MessageMenu isOpen={isOpen} userID={userID} userEmail={userEmail}/>
      </div>}
    </>
  );
};

export default ChatPage;
