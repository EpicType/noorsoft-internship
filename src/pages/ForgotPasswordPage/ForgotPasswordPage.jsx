import React from "react";

import s from "./ForgotPasswordPage.module.scss";

import AuthWrapper from "../../components/AuthComponents/AuthWrapper/AuthWrapper";
import AuthHeader from "../../components/AuthComponents/AuthHeader/AuthHeader";
import AuthInput from "../../components/AuthComponents/AuthInput/AuthInput";
import {useForm} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import SubmitButton from "../../components/AuthComponents/SubmitButton/SubmitButton";
import {NavLink, useHistory} from "react-router-dom";
import {LOGIN_PAGE, REGISTRATION_PAGE} from "../../router/consts";
import {SET_FORGOT_PASSWORD_REQUEST} from "../../store/reducers/auth/consts";


const ForgotPasswordPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const isFetching = useSelector(state => state.auth.isFetching);
  const successMessage = useSelector(state => state.auth.successMessage);
  const errorMessage = useSelector(state => state.auth.errorMessage);

  const {
    register,
    handleSubmit,
    formState:{
      errors
    }
  } = useForm({
    mode:"onSubmit"
  });

  const onSubmit = (data) => {
    dispatch({type: SET_FORGOT_PASSWORD_REQUEST, payload: data.email, history})
  };

  return (
    <AuthWrapper>
      <AuthHeader
        desc="Reset your password"
      />
      <div className={s.error_container}>
        {successMessage && <p className={s.success_container}>{successMessage}</p>}
        {errorMessage && <p>{errorMessage}</p>}
      </div>
      <form className={s.forgotPasswordForm} onSubmit={handleSubmit(onSubmit)}>
        <AuthInput
          type="email"
          placeholder="Email"
          register={register}
          options={
            {
              ...register("email", {
                required:"Enter email",
                pattern:{
                  value:/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message:"Incorrect email address"
                }
              })
            }}
        />
        <div className={s.error_container}>
          {errors.email && <p>{errors.email?.message}</p>}
        </div>
        <SubmitButton isLoading={isFetching} text="Send an email"/>
      </form>
      <div className={s.footer}>
        <NavLink to={LOGIN_PAGE} className={s.footer__link}>Sign In</NavLink>
        <NavLink to={REGISTRATION_PAGE} className={s.footer__link}>Sign Up</NavLink>
      </div>
    </AuthWrapper>
  );
};

export default ForgotPasswordPage;
