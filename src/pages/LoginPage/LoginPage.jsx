import React, {useEffect} from "react";
import {useForm} from "react-hook-form";
import {useDispatch, useSelector} from "react-redux";
import {NavLink, useHistory} from "react-router-dom";

import s from "./LoginPage.module.scss";

import {FORGOT_PASSWORD_PAGE, REGISTRATION_PAGE} from "../../router/consts";
import {
  SET_ERROR_MESSAGE,
  SET_LOGIN_FAILURE,
  SET_LOGIN_REQUEST,
  SET_SUCCESS_MESSAGE
} from "../../store/reducers/auth/consts";

import FormFooter from "../../components/AuthComponents/FormFooter/FormFooter";
import SubmitButton from "../../components/AuthComponents/SubmitButton/SubmitButton";
import AuthInput from "../../components/AuthComponents/AuthInput/AuthInput";
import AuthHeader from "../../components/AuthComponents/AuthHeader/AuthHeader";
import AuthWrapper from "../../components/AuthComponents/AuthWrapper/AuthWrapper";

const LoginPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const isFetching = useSelector(state => state.auth.isFetching);
  const isError = useSelector(state => state.auth.errorMessage);
  const successMessage = useSelector(state => state.auth.successMessage);

  useEffect(() => {
    dispatch({type:SET_ERROR_MESSAGE, payload:""});
    dispatch({type:SET_SUCCESS_MESSAGE, payload:""});
  }, []);

  const {
    register,
    handleSubmit,
    formState:{
      errors
    }
  } = useForm({
    mode:"onSubmit"
  });

  const onSubmit = (data) => {
    dispatch({type:SET_LOGIN_REQUEST, payload:data, history});
  };

  return (
    <AuthWrapper>
      <AuthHeader
        desc="Please Sign In to your account !"
      />
      <div className={s.error_container}>
        {isError && <p className={s.loginInput__error}>{isError}</p>}
        {successMessage && <p className={s.loginInput__succesReg}>{successMessage}</p>}
      </div>
      <form className={s.loginForm} onSubmit={handleSubmit(onSubmit)}>
        <AuthInput
          type="email"
          placeholder="Email"
          register={register}
          options={
            {
              ...register("email", {
                required:"Enter email",
                pattern:{
                  value:/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message:"Incorrect email address"
                }
              })
            }}
        />
        <div className={s.error_container}>
          {errors.email && <p className={s.loginInput__error}>{errors.email?.message}</p>}
        </div>
        <AuthInput
          type="password"
          placeholder="Password"
          register={register}
          options={
            {
              ...register("password", {
                required:"Enter password", minLength:{
                  value:5, message:"The minimum password length is 5 characters"
                }
              })
            }
          }
        />
        <div className={s.error_container}>
          <NavLink to={FORGOT_PASSWORD_PAGE} className={s.loginInput__forgot}>Forgot password ?</NavLink>
          {errors.password && <p className={s.loginInput__error}>{errors.password?.message}</p>}
        </div>
        <SubmitButton isLoading={isFetching} text="Login"/>
      </form>
      <FormFooter
        text="New on our platform ?"
        textLink="Create an account"
        path={REGISTRATION_PAGE}
      />
    </AuthWrapper>
  );
};

export default LoginPage;
