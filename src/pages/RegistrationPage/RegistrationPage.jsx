import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {useForm} from "react-hook-form";

import s from "./RegistrationPage.module.scss";

import {LOGIN_PAGE} from "../../router/consts";
import {SET_ERROR_MESSAGE, SET_REGISTRATION_REQUEST, SET_REGISTRATION_SUCCESS} from "../../store/reducers/auth/consts";
import FormFooter from "../../components/AuthComponents/FormFooter/FormFooter";
import SubmitButton from "../../components/AuthComponents/SubmitButton/SubmitButton";
import AuthHeader from "../../components/AuthComponents/AuthHeader/AuthHeader";
import AuthInput from "../../components/AuthComponents/AuthInput/AuthInput";
import AuthWrapper from "../../components/AuthComponents/AuthWrapper/AuthWrapper";

const RegistrationPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const isError = useSelector(state => state.auth.errorMessage);
  const isFetching = useSelector(state => state.auth.isFetching);

  useEffect(() => {
    dispatch({type: SET_REGISTRATION_SUCCESS, payload: ""});
    dispatch({type: SET_ERROR_MESSAGE, payload: ""});
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    formState: {
      errors
    }
  } = useForm({
    mode: "onChange"
  });

  const watchPassword = watch("password", false);

  const onSubmit = (data) => {
    dispatch({type: SET_REGISTRATION_REQUEST, payload: data, history});
  };

  return (
    <AuthWrapper>
      <AuthHeader desc="Register and start using this wonderful application."/>
      <div className={s.error_container}>
        {isError && <p className={s.registrationInput__error}>{isError}</p>}
      </div>
      <form className={s.registrationForm} onSubmit={handleSubmit(onSubmit)}>
        <AuthInput
          type="email"
          placeholder="Email"
          register={register}
          options={
            {
              ...register("email", {
                required: "Enter email",
                pattern: {
                  value: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                  message: "Incorrect email address"
                }
              })
            }}
        />
        <div className={s.error_container}>
          {errors.email && <p className={s.registrationInput__error}>{errors.email?.message}</p>}
        </div>
        <AuthInput
          type="password"
          placeholder="Password"
          register={register}
          options={
            {
              ...register("password", {
                required: "Enter password", minLength: {
                  value: 5, message: "The minimum password length is 5 characters"
                }
              })
            }
          }
        />
        <div className={s.error_container}>
          {errors.password && <p className={s.registrationInput__error}>{errors.password?.message}</p>}
        </div>
        <AuthInput
          type="password"
          placeholder="Confirm your password"
          register={register}
          options={
            {
              ...register("confirm_password", {
                required: "Confirm password", validate: value => value === watchPassword || "The password do not match"
              })
            }
          }
        />
        <div className={s.error_container}>
          {errors.confirm_password &&
          <p className={s.registrationInput__error}>{errors.confirm_password?.message}</p>}
        </div>
        <SubmitButton isLoading={isFetching} text="Sign Up"/>
      </form>
      <FormFooter
        text="Already have an account ?"
        textLink="Sign In instead"
        path={LOGIN_PAGE}
      />
    </AuthWrapper>
  );
};

export default RegistrationPage;
