export const REGISTRATION_PAGE = "/registration"
export const LOGIN_PAGE = "/login";
export const CHAT_PAGE = "/";
export const FORGOT_PASSWORD_PAGE = "/password_reset";
