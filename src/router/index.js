import {CHAT_PAGE, FORGOT_PASSWORD_PAGE, LOGIN_PAGE, REGISTRATION_PAGE} from "./consts";
import LoginPage from "../pages/LoginPage/LoginPage";
import ChatPage from "../pages/ChatPage/ChatPage";
import RegistrationPage from "../pages/RegistrationPage/RegistrationPage";
import ForgotPasswordPage from "../pages/ForgotPasswordPage/ForgotPasswordPage";

export const routes = [
  {
    exact: true,
    path: LOGIN_PAGE,
    component: LoginPage,
  },
  {
    exact: true,
    path: REGISTRATION_PAGE,
    component: RegistrationPage,
  },
  {
    exact: true,
    path: FORGOT_PASSWORD_PAGE,
    component: ForgotPasswordPage
  },
  {
    exact: false,
    path: CHAT_PAGE,
    component: ChatPage,
  }
];
