import {call, put, takeLatest, delay} from "redux-saga/effects";
import {
  SET_EXIT_REQUEST,
  SET_FETCHING,
  SET_ERROR_MESSAGE,
  SET_LOGIN_REQUEST,
  SET_LOGIN_SUCCESS,
  SET_REGISTRATION_REQUEST,
  SET_SUCCESS_MESSAGE,
  SET_USER_ID, SET_FORGOT_PASSWORD_REQUEST
} from "../store/reducers/auth/consts";
import AuthService from "../services/AuthService";
import {toast} from "react-toastify";
import {db} from "../firebase/firebase";


export function* loginWorker(action) {
  debugger;
  try {
    toast.info("Loading");
    yield put({type:SET_ERROR_MESSAGE, payload:""});
    yield put({type:SET_SUCCESS_MESSAGE, payload:""});
    yield put({type:SET_FETCHING, payload:true});
    const {email, password} = action.payload;
    const history = action.history;
    const response = yield call(AuthService.signIn, email, password);
    if (response) {
      const user = response.response.user;
      yield put({type:SET_USER_ID, payload:user.uid});
      yield put({type:SET_LOGIN_SUCCESS, payload:user.email});
      toast.success("Successful login");
      history.push("/");
    }
  } catch (e) {
    toast.error("error");
    if (e.code === "auth/invalid-email" || e.code === "auth/wrong-password") {
      yield put({type:SET_ERROR_MESSAGE, payload:"Wrong email or password."});
    } else if (e.code === "auth/user-disabled") {
      yield put({type:SET_ERROR_MESSAGE, payload:"Your account has been disabled."});
    } else if (e.code === "auth/user-not-found") {
      yield put({type:SET_ERROR_MESSAGE, payload:"The user with such data was not found."});
    } else {
      yield put({type:SET_ERROR_MESSAGE, payload:e.message});
    }
  } finally {
    yield put({type:SET_FETCHING, payload:false});
  }
}


export function* registrationWorker(action) {
  debugger;
  try {
    debugger;
    toast.info("Loading");
    yield put({type:SET_ERROR_MESSAGE, payload:""});
    yield put({type:SET_FETCHING, payload:true});
    const {email, password} = action.payload;
    // const history = action.history;
    const response = yield call(AuthService.registration, email, password);
    if (response) {
      const user = response.response.user;
      const userRef = db.ref('users').child(user.uid)
      userRef.set({
        displayName: "",
        email: user.email,
        photoURL: "",
        uid: user.uid,
      })
      toast.success("Successful registration");
      yield call(loginWorker, action)
    }
  } catch (e) {
    toast.error("error");
    if (e.code === "auth/email-already-in-use") {
      yield put({type:SET_ERROR_MESSAGE, payload:"This mail is already in use."});
    } else if (e.code === "auth/invalid-email") {
      yield put({type:SET_ERROR_MESSAGE, payload:"This email address is not valid."});
    } else if (e.code === "auth/weak-password") {
      yield put({type:SET_ERROR_MESSAGE, payload:"The password is too weak."});
    } else if (e.code === "auth/operation-not-allowed") {
      yield put({type:SET_ERROR_MESSAGE, payload:"Operation is not allowed."});
    } else {
      yield put({type:SET_ERROR_MESSAGE, payload:e.message});
    }
  } finally {
    yield put({type:SET_FETCHING, payload:false});
  }
}

export function* forgotPasswordWorker(action) {
  debugger;
  try {
    yield put({type:SET_ERROR_MESSAGE, payload:""});
    yield put({type:SET_SUCCESS_MESSAGE, payload:""});
    yield put({type:SET_FETCHING, payload:true});
    const email = action.payload;
    const history = action.history
    yield call(AuthService.forgotPassword, email);
    yield put({type: SET_SUCCESS_MESSAGE, payload: "The email was sent successfully. Go to the mail and follow the link."})
    toast.success("After 5 seconds, you will be redirected to the Login page", {draggable: true, autoClose: 5000})
    yield delay(5000);
    history.push("/login");
  } catch (e) {
    toast.error("error");
    if (e.code === "auth/invalid-email") {
      yield put({type: SET_ERROR_MESSAGE, payload: "Email address is not valid."})
    } else if (e.code === "auth/user-not-found") {
      yield put({type: SET_ERROR_MESSAGE, payload: "No user corresponding to the email address."})
    }
  } finally {
    yield put({type:SET_FETCHING, payload:false});
  }
}




export function* exitWorker(action) {
  debugger;
  try {
    yield call(AuthService.logout);
  } catch (e) {

  } finally {

  }
}



export function* authWatcher() {
  yield takeLatest(SET_LOGIN_REQUEST, loginWorker);
  yield takeLatest(SET_REGISTRATION_REQUEST, registrationWorker);
  yield takeLatest(SET_EXIT_REQUEST, exitWorker);
  yield takeLatest(SET_FORGOT_PASSWORD_REQUEST, forgotPasswordWorker);
}
