import {db} from "../firebase/firebase";
import firebase from "firebase/compat";
import {put, takeLatest} from "redux-saga/effects";
import {
  GET_DIALOGS_REQUEST,
  GET_DIALOGS_SUCCESS,
  SEND_MESSAGE_REQUEST,
  SEND_MESSAGE_SUCCESS
} from "../store/reducers/chat/consts";
import {editLoadingStatus, setDialogsList} from "../store/reducers/chat/action-creators";
import {IDialog, IMessage} from "../types/types";

export function* sendMessageWorker(action?: any): Generator {
  try {
    debugger;
    const {dialogID, userID, inputValue, userAvatar, userName} = action.payload;
    console.log(action.payload);
    debugger;
    const newMessageKey: any = db.ref("/dialogsMessage/" + dialogID + "/messages/").push().key;

    const message: IMessage = {
      chat_type: "group",
      content_type: "text",
      from_id: userID,
      from_name: userName,
      message: inputValue,
      userAvatar: userAvatar,
      dialogID: dialogID,
      startedAt: firebase.database.ServerValue.TIMESTAMP,
      _id: newMessageKey,
    };
    console.log("Ключ", newMessageKey)
    db.ref("/dialogsMessage/" + dialogID + "/messages/" + newMessageKey).update(message);
    // const updates: any = {}
    // updates["/dialogsMessage/" + dialogID + newMessageKey] = message;
    // updates["/dialogsMessage/" + newMessageKey] = newMessageKey;
    // updates["/dialogsMessage/" + dialogID + newMessageKey] = newMessageKey;
    // const {response, error}: any = yield call(ChatService.sendMessage, dialogID, message);
    // db.ref("/dialogs/" + dialogID + "/recentMessage/").set(message)
      // yield put(sendMessageSuccess(message))
      yield put({type: SEND_MESSAGE_SUCCESS, payload: message})
    // if (!!error) {
    //   yield put(sendMessageFailure(error.message))
      // alert("ОШИБКА", error.message);
    // }
  } catch (err) {
    // alert("ОШИБКА", err);
  } finally {

  }
}

export function* getDialogListWorker(action: any): Generator {
  debugger;
  try {
    yield put(editLoadingStatus(true));
    const {status, userID} = action.payload;
    // db.ref("dialogs").orderByChild("operatorId").equalTo(null).on("value", snapshot => {
    //   let dialogs = [];
    //   snapshot.forEach((snap) => {
    //     dialogs.push(snap.val());
    //   });
    //   // dispatch(setDialogsList(dialogs));
    //   console.log("DIALOGS LIST", dialogs);
    // })

    let dialogsList: any = [];

    const dialogs: any = yield new Promise((resolve) =>
      db.ref("dialogs").orderByChild("operatorId").equalTo(userID).on("value", resolve)
    );
    dialogs.forEach((snap: any) => {
      dialogsList.push(snap.val());
    })


    let filteredDialogs: any = [];

    if (status === "saved") {
      filteredDialogs = dialogsList.filter((dialog: any) => dialog.saved === true)
    } else if (status === "all") {
      const dialogs: any = yield new Promise((resolve) =>
        db.ref("dialogs").orderByChild("operatorId").equalTo(null).on("value", resolve)
      )
      dialogs.forEach((snap: any) => {
        filteredDialogs.push(snap.val())
      });
    } else {
      filteredDialogs = dialogsList.filter((dialog: any) => dialog.status === status);
    }

    // yield put(setDialogsList(filteredDialogs))



    //
    // if (status === "saved") {
    //   const dialogs: any = yield new Promise((resolve) =>
    //     db.ref("dialogs").orderByChild("saved").equalTo(true).on("value", resolve)
    //   );
    //   dialogs.forEach((snap: any) => {
    //     dialogsList.push(snap.val());
    //   });
    //
    // } else {
    //   const dialogs: any = yield new Promise((resolve) =>
    //     db.ref("dialogs").orderByChild("status").equalTo(status).on("value", resolve)
    //   );
    //   dialogs.forEach((snap: any) => {
    //     dialogsList.push(snap.val());
    //   });
    // }
    // yield put(setDialogsList(dialogsList));
    // yield put(editLoadingStatus(false));
    console.log("Отфильтрованные по статусу диалоги",filteredDialogs);


    yield put({type: GET_DIALOGS_SUCCESS, payload: filteredDialogs})
    // yield put({type: GET_DIALOGS_SUCCESS, payload: dialogsList})

    // let dialogsID = [];
    // debugger;
    // dialogsList.map(el => (
    //   dialogsID.push(el._id)
    // ))
    // console.log(dialogsID)

    // const allMessage = [];
    //
    // const promise = dialogsID.map(id => {
    //   return db.ref("/dialogsMessage/").child(id).once("value")
    // })
    // Promise.all(promise).then((snapshot) => {
    //   snapshot.forEach((snap) => {
    //     allMessage.push(snap.val())
    //   })
    //   console.log("ВСЕ СООБЩЕНИЯ", allMessage)
    // })

    // const channel = new eventChannel(emiter => {
    //   const listener = db.ref("dialogs").orderByChild("status").equalTo(status).on("value", snapshot => {
    //     emiter({
    //       data: snapshot.val() || {}
    //     });
    //
    //     return () => {
    //       db.ref("dialogs").off();
    //     }
    //   });
    // });
    // // return listener;
    // const {data} = yield take(channel)
    // console.log("ДАННЫЕ", data);

  } catch (e) {

  } finally {

  }
}


export function* chatWatcher() {
  yield takeLatest(SEND_MESSAGE_REQUEST, sendMessageWorker);
  yield takeLatest(GET_DIALOGS_REQUEST, getDialogListWorker);
}
