import {all} from "redux-saga/effects";
import {authWatcher} from "./auth.saga";
import {chatWatcher} from "./chat.saga";

export default function* rootSaga() {
  yield all([
    authWatcher(),
    chatWatcher()
  ])
}
