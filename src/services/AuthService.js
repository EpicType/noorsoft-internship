import {auth} from "../firebase/firebase";

export default class AuthService {
  static signIn(email, password) {
    return auth.signInWithEmailAndPassword(email, password)
      .then(response => ({response}))
  }

  static logout() {
    return auth.signOut();
  }

  static registration(email, password) {
    return auth.createUserWithEmailAndPassword(email, password)
      .then(response => ({response}))
  }

  static forgotPassword(email) {
    return auth.sendPasswordResetEmail(email)
  }
}
