import {db} from "../firebase/firebase";

export default class ChatService {
  static getDialogs(userID) {
    return db.ref("/dialogs/" + userID)
  }
  static sendMessage(dialogID, message) {
    return db.ref("/dialogsMessage/" + dialogID).push(message)
      .then(response => ({response}))
      .catch(error => ({error}))
  }
}
