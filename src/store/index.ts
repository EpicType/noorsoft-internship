import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, combineReducers, createStore} from "redux";
import authReducer from "./reducers/auth/reducer";
import rootSaga from "../sagas";
import chatReducer from "./reducers/chat/reducer";

const sagaMiddleware = createSagaMiddleware();

const rootReducers = combineReducers({
  auth: authReducer,
  chat: chatReducer,
})

export type RootState = ReturnType<typeof rootReducers>;

const store = createStore(rootReducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
