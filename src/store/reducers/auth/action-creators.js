import {SET_AUTH, SET_FETCHING, SET_USER_EMAIL, SET_USER_ID, SET_USER_NAME, SET_USER_PHOTO_URL} from "./consts";

export const setAuth = (bool) => {
  return {
    type: SET_AUTH,
    payload: bool,
  };
};

export const setFetching = (bool) => {
  return {
    type: SET_FETCHING,
    payload: bool,
  };
};

export const setUserEmail = (userEmail) => {
  return {
    type: SET_USER_EMAIL,
    payload: userEmail,
  };
};

export const setUserName = (userName) => {
  return {
    type: SET_USER_NAME,
    payload: userName
  };
};

export const setUserID = (id) => {
  return {
    type: SET_USER_ID,
    payload: id,
  };
};

export const setUserPhoto = (photoURL) => {
  return {
    type: SET_USER_PHOTO_URL,
    payload: photoURL,
  };
};
