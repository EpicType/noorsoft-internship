import {
  SET_AUTH,
  SET_ERROR_MESSAGE,
  SET_FETCHING,
  SET_LOGIN_SUCCESS,
  SET_SUCCESS_MESSAGE,
  SET_USER_EMAIL,
  SET_USER_ID, SET_USER_NAME,
  SET_USER_PHOTO_URL
} from "./consts";

const initialState = {
  isAuth:false,
  isFetching:false,
  errorMessage:"",
  successMessage:"",
  userEmail:"",
  photoURL:"",
  userID:null,
  userName: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_SUCCESS: {
      return {
        ...state,
        userEmail:action.payload,
        isAuth:true,
      };
    }
    case SET_SUCCESS_MESSAGE: {
      return {
        ...state,
        successMessage:action.payload,
      };
    }
    case SET_ERROR_MESSAGE: {
      return {
        ...state,
        errorMessage:action.payload
      };
    }
    case SET_USER_PHOTO_URL: {
      return {
        ...state,
        photoURL:action.payload
      };
    }

    case SET_USER_NAME: {
      return {
        ...state,
        userName: action.payload
      }
    }
    // case SET_LOGIN_FAILURE: {
    //   return {
    //     ...state,
    //     errorMessage:action.payload,
    //   };
    // }
    case SET_AUTH: {
      return {
        ...state,
        isAuth:action.payload,
      };
    }
    case SET_FETCHING: {
      return {
        ...state,
        isFetching:action.payload,
      };
    }
    case SET_USER_EMAIL: {
      return {
        ...state,
        userEmail:action.payload,
      };
    }
    case SET_USER_ID: {
      return {
        ...state,
        userID:action.payload
      };
    }
    default:
      return state;
  }
};

export default authReducer;
