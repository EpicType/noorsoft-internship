import {
  EDIT_DIALOG_STATUS,
  EDIT_LOADING_STATUS, SEND_MESSAGE_FAILURE, SEND_MESSAGE_SUCCESS,
  SET_ACTIVE_DIALOGS,
  SET_DIALOGS_SUCCESS, SET_FOUNDED_DIALOGS_LIST, SET_FOUNDED_MESSAGES_LIST,
  SET_IS_OPEN,
  SET_MESSAGES_LIST
} from "./consts";

export const setDialogsList = (dialogs) => {
  return {
    type: SET_DIALOGS_SUCCESS,
    payload: dialogs,
  };
};

export const setFoundedDialogsList = (dialogs) => {
  return {
    type: SET_FOUNDED_DIALOGS_LIST,
    payload: dialogs
  }
}

export const setFoundedMessagesList = (messages) => {
  return {
    type: SET_FOUNDED_MESSAGES_LIST,
    payload: messages
  }
}

export const setMessagesList = (messages) => {
  return {
    type: SET_MESSAGES_LIST,
    payload: messages,
  };
};

// export const setActiveDialogs = (status) => {
//   return {
//     type: SET_ACTIVE_DIALOGS,
//     payload: status
//   };
// };

export const sendMessageSuccess = (message) => {
  return {
    type: SEND_MESSAGE_SUCCESS,
    payload: message,
  }
}

export const sendMessageFailure = (error) => {
  return {
    type: SEND_MESSAGE_FAILURE,
    payload: error
  }
}

export const editDialogStatus = (dialogID, newDialogStatus) => {
  debugger;
  return {
    type: EDIT_DIALOG_STATUS,
    payload: {dialogID, newDialogStatus}
  };
};

export const editIsOpen = (bool) => {
  return {
    type: SET_IS_OPEN,
    payload: bool,
  };
};


export const editLoadingStatus = (bool) => {
  return {
    type: EDIT_LOADING_STATUS,
    payload: bool
  };
};
