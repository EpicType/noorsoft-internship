import {
  EDIT_DIALOG_STATUS,
  EDIT_LOADING_STATUS,
  GET_DIALOGS_SUCCESS,
  SEND_MESSAGE_FAILURE,
  SEND_MESSAGE_SUCCESS,
  SET_FOUNDED_DIALOGS_LIST,
  SET_FOUNDED_MESSAGES_LIST,
  SET_IS_OPEN,
  SET_MESSAGES_LIST
} from "./consts";


const initialState = {
  dialogsList: [],
  filteredDialogsList: [],
  foundedDialogsList: [],
  foundedMessagesList: [],
  messagesList: [],
  errorMessage: "",
  isLoading: false,
  isOpen: false,
};

const chatReducer = (state = initialState, action) => {
  debugger
  switch (action.type) {
    case GET_DIALOGS_SUCCESS: {
      debugger;
      return {
        ...state,
        dialogsList: action.payload,
        isLoading: false,
      };
    }




    case EDIT_LOADING_STATUS: {
      return {
        ...state,
        isLoading: action.payload
      };
    }


    // case SET_ACTIVE_DIALOGS: {
    //   debugger;
    //   return {
    //     ...state,
    //     filteredDialogsList: state.dialogsList.filter((el) => el.status === action.payload)
    //   };
    // }
    case SET_IS_OPEN: {
      return {
        ...state,
        isOpen: action.payload
      };
    }

    case SET_MESSAGES_LIST: {
      return {
        ...state,
        messagesList: action.payload,
      };
    }

    case SEND_MESSAGE_SUCCESS: {
      return {
        ...state,
        messagesList: [...state.messagesList, action.payload]
      };
    }
    case SEND_MESSAGE_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload,
      };
    }

    case SET_FOUNDED_DIALOGS_LIST: {
      return {
        ...state,
        foundedDialogsList: action.payload,
      };
    }

    case SET_FOUNDED_MESSAGES_LIST: {
      return {
        ...state,
        foundedMessagesList: action.payload
      };
    }


    case EDIT_DIALOG_STATUS: {
      debugger;
      return {
        ...state,
        dialogsList: state.dialogsList.map((el) => el._id === action.payload.dialogID ? {
          ...el,
          saved: action.payload.newDialogStatus
        } : el)
      };
    }


    default:
      return state;
  }
};

export default chatReducer;

