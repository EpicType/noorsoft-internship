export interface IMessage {
  chat_type: string,
  content_type: string,
  dialogID: string,
  from_id: string,
  from_name: string,
  message: string,
  startedAt: number | Object,
  userAvatar: string,
  _id: string | null,
}

export interface IDialog {
  _id: string,
  createdAt: string,
  name: string,
  operatorId: string,
  operatorName: string,
  recentMessage: IMessage,
  requestText: string,
  saved: boolean,
  star: number,
  status: string,
  userId: string,
  userName: string,
}
